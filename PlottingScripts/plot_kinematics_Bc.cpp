#include "tools.h"

void plot_kinematics_Bc(){


    SetLHCbStyle("oneD");
    static const double megaelectronvolt = 1;
    static const double gigaelectronvolt = 1e+3*megaelectronvolt;

    
    // -------------------------------------------------------------------------
    // Get root file - real data
    // -------------------------------------------------------------------------

    TFile* root_file = TFile::Open("root-files/Bc/DVnTuple_Run1.root");
    TTree* root_tree = (TTree*) root_file->Get("TupleBc2JpsiHBDTLine/DecayTree");

    
    root_tree->SetAlias("B_c_ETA","acosh(B_c_P/B_c_PT)");
    root_tree->SetAlias("B_c_Y","0.5*log( (B_c_PE+B_c_PZ) / (B_c_PE-B_c_PZ) )");

    
    int n_entries = root_tree->GetEntries();
    std::cout << "\t" << n_entries << "/" << n_entries << std::endl;

    // ==========================
    // pT distribution for Bc 
    // =========================== 
    TCanvas* c_pT = new TCanvas("c_pT","c_pT");
    TH1D* h_pT  = new TH1D("h_pT", "h_pT",40,0,40);
    root_tree->Draw("B_c_PT*1e-3>>h_pT","" );      //*1e-3 is to convert MeV to GeV
    //h_pT->GetXaxis()->SetMaxDigits(4);    
    h_pT->SetTitle(";p_{T} [GeV/c] ;  ");
    h_pT->SetMinimum(0.0);
    h_pT->Draw("hist");
    c_pT->Print("plots/pT_Bc.pdf");  

    // ==========================
    // eta distribution for Bc 
    // =========================== 
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta  = new TH1D("h_eta", "h_eta",50,0, 6.0);
    root_tree->Draw("B_c_ETA>>h_eta","" );    
    h_eta->SetTitle(";#eta ;  ");
    h_eta->SetMinimum(0.0);
    h_eta->Draw("hist");
    c_eta->Print("plots/eta_Bc.pdf");  

    // ==========================
    // Mass distribution for Bc 
    // =========================== 
    TCanvas* c_Mass = new TCanvas("c_Mass","c_Mass");
    TH1D* h_Mass  = new TH1D("h_Mass", "h_Mass",50,5.5,7.0);
    root_tree->Draw("B_c_MM*1e-3>>h_Mass","" );     //*1e-3 is to convert MeV to GeV
    h_Mass->SetTitle(";Measured Mass [GeV] ;  ");
    h_Mass->SetMinimum(0.0);
    h_Mass->Draw("hist");
    c_Mass->Print("plots/Mass_Bc.pdf");  


    SetLHCbStyle("cont");

    // ==========================
    // eta-pT 2D Histo
    // ===========================    
    TCanvas* c_eta_pT = new TCanvas("c_eta_pT","c_eta_pT");
    TH2D* h_eta_pT  = new TH2D("h_eta_pT", "h_eta_pT",20,0,6,20,0,25);
    root_tree->Draw("B_c_PT*1e-3:B_c_ETA >> h_eta_pT", "");    //*1e-3 is to convert MeV to GeV
    //h_eta_pT->GetYaxis()->SetMaxDigits(3);
    h_eta_pT->SetTitle(";#eta;p_{T} [GeV/c]"); 
    h_eta_pT->SetMinimum(0.0);
    h_eta_pT->Draw("colz");
    c_eta_pT->Print("plots/Eta_pT_Bc.pdf");
}