# BcXbXc_LHCb

1. create Bc from Jpsi and Pi for Run1 and Run2. Files used were JPsiPi2Bc_Data.py (or Bc_Data.py if you do not want to create Bc from scratch and use stripping line line Bc2JpsiHBDTline).

2. to run  the above on grid use ganag_Data.py.

3. to do the step1 and step2 on MC data you can use Bc_MC.py and ganga_MC.py

4. apply prelimenary cuts on the outputs using cuts.py and prelCuts.py

5. train BDT using signal from MC and background from real data with Bc mass cut for >6350MeV/c2. Files used were TMVAClassification.cpp and TMVAClassificationApplication.cpp

6. decide on a BDT response value and apply that as a cut before fitting the Bc mass using MassFitBc.cpp

7. At any stage to plot the tuples you can use the files in Plotting script directory

## the script to make Bc+Xc is BcXc.py, can be used for the Pythia8 Project
