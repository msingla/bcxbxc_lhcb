# Inspired from B2Kstarmumu Analysis Script


name = "Bc_MC_"

years = [
        '2012',  
        ]

pols  = [
        'Down',
        'Up',
        ] 


bkkpaths = { 
    '2012' : { 
                'Down' : {
                           
                            'Bc2JpsiPi'   : { 'path'   : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09l/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/14143013/ALLSTREAMS.DST',
                                              'CondDB' : 'sim-20160321-2-vc-md100'  , 
                                              'DDDB'   : 'dddb-20170721-2' 
                                             },

                          },
                'Up'   : {
                            
                            'Bc2JpsiPi'   : {'path'   : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09l/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/14143013/ALLSTREAMS.DST',
                                             'CondDB' : 'sim-20160321-2-vc-mu100'   , 
                                             'DDDB'   : 'dddb-20170721-2' 

                                             },

                          
                },
            },
        }




myBackend = Dirac()
# myBackend = Local()

### Splitter
mySplitter = SplitByFiles()
mySplitter.filesPerJob = 20
mySplitter.maxFiles = -1

## Testing
# mySplitter.filesPerJob = 1
# mySplitter.maxFiles = 5
for year in years:
    for pol in pols:
        print("Submiting year: %s - polarity: %s" % (year, pol))
        
        bkkpath = bkkpaths[year][pol]
        
        for production, configuration in bkkpath.items():

                print('Production: %s'% (production             ) )
                print('    path:   %s'% (configuration['path']  ) )
                print('    CondDB: %s'% (configuration['CondDB']) )
                print('    DDDB:   %s'% (configuration['DDDB']  ) )

                myJobName = name+year+"_"+pol+"_"+production
                myOutputdata =[DiracFile('*.root')]
                myJobComment = year+"_"+pol

                
                bkkquery = BKQuery(configuration['path'],dqflag=['OK'])
                data = bkkquery.getDataset()
                if not data:
                        print("No data for ",configuration['path'])
                        break
                
                # MC_filename = 'MC_Bc2JpsiPi_%s_%s_Mag%s.root' % (production,year,pol)

                myApplication = GaudiExec()
                myApplication.directory = '/afs/cern.ch/work/m/msingla/Bc_reconstruction/DaVinciDev_v46r2' #Use a recent version of DaVinci as we are just reading a DST
                myApplication.platform = 'x86_64_v2-centos7-gcc11-opt'
                myApplication.options = ["Bc_Run1_MC.py"] # This is the name of your options file #Bc_Run1Data.py is to filter Bc from Run1 Data
                # myApplication.options = ["BcXc.py"]  
                # data = data[0:10] # Just for testing, comment out to send run over all the data
                myApplication.extraOpts = (
                                        "from Configurables import DaVinci  \n"
                                        "DaVinci().CondDBtag = '{conddb}'   \n"
                                        "DaVinci().DDDBtag   = '{dddb}'     \n"
                                        "DaVinci().DataType  = '{year}'     \n"
                                        "DaVinci().TupleFile = 'MC_Bc2JpsiPi_{year}_Mag{pol}.root' \n"
                                        ).format(       pol = pol,
                                                        conddb   = configuration['CondDB'] ,
                                                        dddb     = configuration['DDDB']   ,
                                                        year     = year                    ,
                 
                                                        )   
                        
                j = Job (
                        name         = '%s_%s'%(year,pol),
                        comment      = myJobName,
                        application  = myApplication,
                        splitter     = mySplitter,
                        # postprocessors       = myMerger,  # I tend to merge things myself, but you can use ganga Mergers to combine the output of ganga subjobs
                        # inputfiles = _inputsandbox,
                        outputfiles   = myOutputdata,
                        backend      = myBackend,
                        inputdata    = data,
                        do_auto_resubmit = True,
                        )
            
                #j.application = myApplication
                
                print("... ",pol," out...")
                #to submit the job
                queues.add(j.submit)

print("... Done!")



