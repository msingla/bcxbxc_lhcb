# # --------------------------    
# # pre-selection cuts for Bc2JpsiHDetachedLine
# # ref https://cds.cern.ch/record/1559270/files/LHCb-ANA-2013-061.pdf
# #  -------------------------


# Bcplus  
cutBcplus = ''
cutBcplus += '(B_cplus_ENDVERTEX_CHI2/B_cplus_ENDVERTEX_NDOF) <9'
cutBcplus += '&& B_cplus_IPCHI2_OWNPV <25'
cutBcplus += '&& B_cplus_TAU> 0.0002' #ns
cutBcplus += '&& B_cplus_PT >0 && B_cplus_PT<20000' #MeV/c
cutBcplus += '&& B_cplus_RAPIDITY > 2.0 && B_cplus_RAPIDITY<4.5 ' #put a cut on rapidity for 2.0-4.5

#Jpsi
cutJpsi = ''
cutJpsi += ' J_psi_1S_M > 3040 && J_psi_1S_M < 3140' #MeV/c2
cutJpsi += '&& (J_psi_1S_ENDVERTEX_CHI2/J_psi_1S_ENDVERTEX_NDOF) <9'
# cutJpsi += '&&  J_psi_1S_DOCAMAX ==20'

# #mu+
cutmuplus = ''
cutmuplus += 'muplus_TRACK_Type >=3 && muplus_isMuon'
cutmuplus += ' && muplus_PIDmu > 0'
cutmuplus += '&& muplus_PT>550' #MeV/c
cutmuplus += '&& muplus_TRACK_CHI2NDOF <3'

# #mu-
cutmuminus =''
cutmuminus += 'muminus_TRACK_Type >=3 && muminus_isMuon'
cutmuminus += '&& muminus_PIDmu > 0'
cutmuminus += '&& muminus_PT>550' #GeV/c
cutmuminus += '&& muminus_TRACK_CHI2NDOF <3'

# #piplus
cutpiplus =''
cutpiplus += 'piplus_PT >1000' #MeV/c
cutpiplus += '&& piplus_TRACK_CHI2NDOF <3'
cutpiplus += '&& piplus_TRACK_GhostProb < 0.4'
cutpiplus += '&& piplus_IPCHI2_OWNPV >9'

# #prelimenary cuts 
prelCuts = cutmuminus  + ' && ' + cutmuplus + ' && ' + cutJpsi  + '&&' + cutpiplus + ' && ' + cutBcplus


