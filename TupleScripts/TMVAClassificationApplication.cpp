/**********************************************************************************
 * Project   : TMVA - a Root-integrated toolkit for multivariate data analysis    *
 * Package   : TMVA                                                               *
 * Exectuable: TMVAClassificationApplication                                      *
 *                                                                                *
 * This macro provides a simple example on how to use the trained classifiers     *
 * within an analysis module                                                      *
 **********************************************************************************/

#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <math.h>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include <TChain.h>
#include "TStopwatch.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

//#include "TMVAGui.C"


using namespace std;
using namespace TMVA;

void TMVAClassificationApplication(TString decay = "Signal", TString dataType = "Data", TString myMethod = "BDTG", TString trainedOn = "MC" ) 
{   
#ifdef __CINT__
   gROOT->ProcessLine( ".O0" ); // turn off optimization in CINT
#endif

   //---------------------------------------------------------------
   std::cout << "==> Start TMVAClassificationApplication" << std::endl;

   TString outFileName = "../root_files/TMVA/BDT/";

   TChain* theTree = new TChain("DecayTree");
   if(decay == "Signal" && dataType == "Data"){ 	  
	theTree->Add("../root_files/PreSelection/DVntuple_Bc_Data_2012.root");
   // theTree->Add("../root_files/PreSelection/DVntuple_Bc_Data_2012_MagUp.root");
	outFileName += "signal_BDTG.root";
   } else {
	cout << "Unknown filename, CRASHING." << endl;
	throw "ERROR";
   }

   // Ouput tree
   TFile *hFile = new TFile(outFileName,"RECREATE");
   TTree* tree = theTree->CloneTree(0);

   // This loads the library
   TMVA::Tools::Instance();

   // --- Create the Reader object
   TMVA::Reader *reader = new TMVA::Reader( "Color:!Silent" );    

   // Create a set of variables and declare them to the reader
   Float_t r_B_cplus_IPCHI2_OWNPV;
   Float_t r_muplus_IPCHI2_OWNPV;
   Float_t r_muminus_IPCHI2_OWNPV;
   Float_t r_J_psi_1S_IPCHI2_OWNPV;
   Float_t r_piplus_IPCHI2_OWNPV;
   Float_t r_muplus_PT;
   Float_t r_muminus_PT;
   Float_t r_J_psi_1S_PT;
   Float_t r_piplus_PT;
   Float_t r_B_cplus_LOKI_DTF_CTAU;
   Float_t r_B_cplus_LOKI_DTF_VCHI2NDOF;
   Float_t r_nVeloTracks;
   Float_t r_nTracks;
   Float_t r_nPVs;
   Float_t r_B_cplus_M;
  

   reader->AddVariable( "B_cplus_IPCHI2_OWNPV := sqrt(B_cplus_IPCHI2_OWNPV)",&r_B_cplus_IPCHI2_OWNPV);
   reader->AddVariable( "muplus_IPCHI2_OWNPV := sqrt(muplus_IPCHI2_OWNPV)",&r_muplus_IPCHI2_OWNPV );
   reader->AddVariable( "muminus_IPCHI2_OWNPV:=sqrt(muminus_IPCHI2_OWNPV)",&r_muminus_IPCHI2_OWNPV );
   reader->AddVariable( "J_psi_1S_IPCHI2_OWNPV:=sqrt(J_psi_1S_IPCHI2_OWNPV)",&r_J_psi_1S_IPCHI2_OWNPV);
   reader->AddVariable( "piplus_IPCHI2_OWNPV:=sqrt(piplus_IPCHI2_OWNPV)",&r_piplus_IPCHI2_OWNPV);
   reader->AddVariable( "muplus_PT",&r_muplus_PT);
   reader->AddVariable( "muminus_PT",&r_muminus_PT);
   reader->AddVariable( "J_psi_1S_PT",&r_J_psi_1S_PT);
   reader->AddVariable( "piplus_PT",&r_piplus_PT);
   reader->AddVariable( "B_cplus_LOKI_DTF_CTAU", &r_B_cplus_LOKI_DTF_CTAU);
   reader->AddVariable( "B_cplus_LOKI_DTF_VCHI2NDOF", &r_B_cplus_LOKI_DTF_VCHI2NDOF);

   reader->AddSpectator( "nVeloTracks", &r_nVeloTracks );
   reader->AddSpectator( "nTracks", &r_nTracks );
   reader->AddSpectator( "nPVs", &r_nPVs );
   reader->AddSpectator( "B_cplus_M" , &r_B_cplus_M);



   // --- Book the MVA methods

   TString dir    = "dataset/weights/";
   TString weightFile = dir + "TMVAClassification" +TString("_")+trainedOn+TString("__")+myMethod+ ".weights.xml" ;
	reader->BookMVA( myMethod, weightFile ); 

   Double_t B_cplus_IPCHI2_OWNPV;
   Double_t muplus_IPCHI2_OWNPV;
   Double_t muminus_IPCHI2_OWNPV;
   Double_t J_psi_1S_IPCHI2_OWNPV;
   Double_t piplus_IPCHI2_OWNPV;
   Double_t muplus_PT;
   Double_t muminus_PT;
   Double_t piplus_PT;
   Double_t J_psi_1S_PT;
   Double_t B_cplus_LOKI_DTF_CTAU;
   Double_t B_cplus_LOKI_DTF_VCHI2NDOF;

   theTree->SetBranchAddress( "B_cplus_IPCHI2_OWNPV",&B_cplus_IPCHI2_OWNPV);
   theTree->SetBranchAddress( "muplus_IPCHI2_OWNPV",&muplus_IPCHI2_OWNPV);
   theTree->SetBranchAddress( "muminus_IPCHI2_OWNPV",&muminus_IPCHI2_OWNPV);
   theTree->SetBranchAddress( "J_psi_1S_IPCHI2_OWNPV",&J_psi_1S_IPCHI2_OWNPV );
   theTree->SetBranchAddress( "piplus_IPCHI2_OWNPV",&piplus_IPCHI2_OWNPV);
   theTree->SetBranchAddress( "muplus_PT",&muplus_PT);
   theTree->SetBranchAddress( "muminus_PT",&muminus_PT);
   theTree->SetBranchAddress( "J_psi_1S_PT",&J_psi_1S_PT);
   theTree->SetBranchAddress( "piplus_PT",&piplus_PT);
   theTree->SetBranchAddress( "B_cplus_LOKI_DTF_CTAU", &B_cplus_LOKI_DTF_CTAU);
   theTree->SetBranchAddress( "B_cplus_LOKI_DTF_VCHI2NDOF", &B_cplus_LOKI_DTF_VCHI2NDOF);
   

   //output file---------------------------------------------------------------------------------------------------------------------------------------
  
   Float_t BDTG_response;
   double BDTG;
   tree->Branch("BDTG_response",&BDTG_response, "BDTG_response/F");
   tree->Branch("BDTG",&BDTG, "BDTG/D");

   std::cout << "--- Processing: " << theTree->GetEntries() << " events" << std::endl;

   TStopwatch sw;
   sw.Start();
   int N = theTree->GetEntries();
   for (Long64_t ievt=0; ievt< N ;ievt++) {

      if (ievt%5000 == 0) std::cout << "--- ... Processing event: " << ievt << std::endl;

      theTree->GetEntry(ievt);


      // r_B_cplus_IPCHI2_OWNPV = sqrt(B_cplus_IPCHI2_OWNPV);
      r_muplus_IPCHI2_OWNPV = sqrt(muplus_IPCHI2_OWNPV);
      r_muminus_IPCHI2_OWNPV = sqrt(muminus_IPCHI2_OWNPV);
      // r_J_psi_1S_IPCHI2_OWNPV = sqrt(J_psi_1S_IPCHI2_OWNPV);
      r_piplus_IPCHI2_OWNPV = sqrt(piplus_IPCHI2_OWNPV);
      r_muplus_PT = muplus_PT;
      r_muminus_PT = muminus_PT;
      r_J_psi_1S_PT = J_psi_1S_PT;
      r_piplus_PT = piplus_PT;
      r_B_cplus_LOKI_DTF_CTAU = B_cplus_LOKI_DTF_CTAU;
      r_B_cplus_LOKI_DTF_VCHI2NDOF = B_cplus_LOKI_DTF_VCHI2NDOF;

      BDTG_response=reader->EvaluateMVA(myMethod);
      BDTG = double(BDTG_response);
      tree->Fill();    
   }

   // Get elapsed time
   sw.Stop();
   std::cout << "--- End of event loop: "; sw.Print();

   tree->Write();
   delete reader;
   hFile->Close();
    
   cout << "Wrote to file: " << outFileName << endl;
   cout << "==> TMVAClassificationApplication is done!" << endl << endl;
} 
