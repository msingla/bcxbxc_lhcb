from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci
from GaudiConf import IOHelper

# Stream and stripping line we want to use
stream = 'Dimuon'
line = 'Bc2JpsiHBDTLine'

# define the StrippingLine line
dtt = DecayTreeTuple('TupleBc2JpsiHBDTLine')
dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)]

# Note that we mark all particles, otherwise the branches won't work
dtt.Decay =   '[ B_c- -> ^J/psi(1S) ^pi- ]CC'

# Add tuple tools
track_tool = dtt.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True
dtt.addTupleTool('TupleToolPrimaries')

dtt.addBranches({
    'B_c': '[ B_c- -> J/psi(1S) pi- ]CC',
    'JPsi1S': '[ B_c- -> ^J/psi(1S) pi- ]CC',
    'piminus': '[ B_c- -> J/psi(1S) ^pi- ]CC',
})

dtt.JPsi1S.addTupleTool('TupleToolPropertime')

# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().Simulation = False
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

DaVinci().DataType = '2012'
DaVinci().CondDBtag = 'cond-20141107'
DaVinci().DDDBtag = 'dddb-20130929-1'

# # Use the local input data
IOHelper().inputFiles([
     './00041836_00000171_1.dimuon.dst',
     './00041836_00000407_1.dimuon.dst',
     './00041836_00000208_1.dimuon.dst'
], clear=True)
