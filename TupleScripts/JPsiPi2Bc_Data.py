from Configurables import DecayTreeTuple
# from Configurables import TupleToolDecayTreeFitter
from Configurables import TupleToolTISTOS
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci
from GaudiConf import IOHelper
from Gaudi.Configuration import *

# Load algorithms
from Configurables import LoKi__HDRFilter

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
from StandardParticles import StdAllLooseProtons as Protons

# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection, StrippingSelection
from PhysConf.Selections import SelectionSequence, AutomaticData, TupleSelection, Selection



triggerlines = [
    "L0SingleMuonDecision",
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0HadronDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    #Hlt1 MVA lines
    'Hlt1TrackMVADecision', 
    'Hlt1TwoTrackMVADecision', 
    'Hlt1TrackMVALooseDecision', 
    'Hlt1TwoTrackMVALooseDecision', 
    #Hlt1 track lines    
    'Hlt1TrackAllL0Decision', #is this superseded? 
    'Hlt1TrackMuonDecision', 
    'Hlt1TrackMuonNoSPDDecision', 
    #Hlt1 muon lines
    'Hlt1DiMuonHighMassDecision', 
    'Hlt1DiMuonLowMassDecision', 
    'Hlt1SingleMuonNoIPDecision', 
    'Hlt1SingleMuonHighPTDecision', 
    'Hlt1MultiMuonNoL0Decision', 
    'Hlt1MultiMuonNoIPDecision', 
    'Hlt1DiMuonNoL0Decision', 
    'Hlt1DiMuonNoIPDecision', 
    #Hlt1 other lines
    'Hlt1L0AnyDecision', 
    'Hlt1L0AnyNoSPDDecision', 
    'Hlt1TrackMuonMVADecision',#does this exist, yes at least for 2016 
    #Hlt2 muon lines
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2SingleMuonRareDecision',
    'Hlt2SingleMuonVHighPTDecision',
    #Hlt2 dimuon lines
    'Hlt2DiMuonDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonDetachedPsi2SDecision'
    'Hlt2DiMuonSoftDecision', #?
    #Hlt2 Too
    'Hlt2TopoMu2BodyBBDTDecision',
    'Hlt2MuTopo3BodyBBDTDecision',
    'Hlt2Topo2BodyDecision', 
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision'
]


# Stream and stripping line we want to uses
stream = 'Dimuon'
line = 'FullDSTDiMuonJpsi2MuMuDetachedLine'

# strip_sel = LoKi__HDRFilter ( 'stripFilter',
#                         Code = "HLT_PASS('StrippingFullDSTDiMuonJpsi2MuMuDetachedLine')",
#                         Location= "/Event/Strip/Phys/DecReports")

# telling the algorithm to get B_c data from the below location
Jpsi_data = AutomaticData('/Event/{0}/Phys/{1}/Particles'.format(stream,line))

# # -----------------------
# # JPsi2mumu
# # -----------------------

# Jpsi_decay = {^J/psi(1S -> ^mu+ ^mu-) }


# -----------------------
# Bc2JpsiPi
# -----------------------

### Bc2JpsiPi:: mother and daughter cuts are taken from StrippingBc2JpsiHDetachedLine

# Build the D0 from the kaons
Bc_decay_products = {
    'pi+': '(PT > 1000.0 *MeV) & (P > -5.0 *MeV) & (TRCHI2DOF < 5.0)',
}

Bc2JpsiPi_sel = CombineSelection(
    'Bc2JpsiPi_sel',
    [Jpsi_data, Pions],
    DecayDescriptor= '[B_c+ -> J/psi(1S) pi+]cc',
    DaughtersCuts= Bc_decay_products,
    CombinationCut="ADAMASS('B_c+') < 400.0 *MeV",
    MotherCut="(VFASPF(VCHI2PDOF)< 9.0 ) & (PT > 0.0 *MeV) & (BPVLTIME()>0.2*ps) & (INTREE( (ABSID=='pi+') & (BPVIPCHI2()>9)))",
)
Bc2JpsiPi_seq = SelectionSequence('Bc2JpsiPi_seq', TopSelection=Bc2JpsiPi_sel)


# -------------------------------------
# Define function to create an ntuple
# ------------------------------------

def create_tuples(name,input, decay):
    dtt = DecayTreeTuple(name)
    dtt.ToolList =  [    "TupleToolGeometry",
                            "TupleToolL0Data",
                            "TupleToolKinematic",
                            "TupleToolPrimaries",
                            "TupleToolEventInfo",
                            "TupleToolTrackInfo",
                            "TupleToolRecoStats",
                            "TupleToolAngles",
                            "TupleToolDira",
                            "TupleToolPid",
                            "TupleToolPropertime",
                            "TupleToolANNPID",
                            ]
    tistos = dtt.addTupleTool(TupleToolTISTOS)
    tistos.TriggerList = triggerlines
    tistos.Verbose = True
    tistos.VerboseL0 = True
    tistos.VerboseHlt1 = True
    tistos.VerboseHlt2 = True

    Decay_LoKi = dtt.addTupleTool('LoKi::Hybrid::TupleTool/Decay_LoKi')
    Decay_LoKi.Variables = { 
            "RAPIDITY"                 : "Y",
            "RAPIDITY0"                : "Y0",
            "ETA"                      : "ETA",
            "PHI"                      : "PHI", 
            "LOKI_DTF_CTAU"            : "DTF_CTAU( 0, True )",
            "LOKI_DTF_CTAUS"           : "DTF_CTAUSIGNIFICANCE( 0, True )",
            "LOKI_DTF_CHI2NDOF"        : "DTF_CHI2NDOF( True )",
            "LOKI_DTF_CTAUERR"         : "DTF_CTAUERR( 0, True )",
            "LOKI_MASS_JpsiConstr"     : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
            "LOKI_DTF_MASS"            : "DTF_FUN ( M , True )" ,
            "LOKI_DTF_VCHI2NDOF"       : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )" ,
            "LOKI_DTF_CHI2_JpsiConstr" : "DTF_CHI2(  True , 'J/psi(1S)' )"
            }
    dtt.Inputs = input
    dtt.Decay  = decay
    return dtt


# ------------------------------------------
# Create ntuples for various decay channels
# ------------------------------------------

Bc2JpsiPi_decay = '[ B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+ ]CC'

Bc2JpsiPi_dtt   = create_tuples('TupleBc2JpsiPi', Bc2JpsiPi_seq.outputLocations(), Bc2JpsiPi_decay )

seq = [
      Bc2JpsiPi_seq.sequence()
      ]

dtt = [
      Bc2JpsiPi_dtt,
      ]

DaVinci().UserAlgorithms += seq
DaVinci().UserAlgorithms += dtt

# DaVinci configuration
# DaVinci().EventPreFilters = [strip_sel]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation       # Only ask for luminosity information when not using simulated data
DaVinci().EvtMax = -1

# comment out the following lines for grid submission as will be read out by the job description file (ganga_Run1Data.py)
DaVinci().DataType = '2012'
DaVinci().CondDBtag = 'cond-20141107'
DaVinci().DDDBtag = 'dddb-20130929-1'

#Use the local input data
IOHelper().inputFiles([
     './00041836_00000171_1.dimuon.dst',
     './00041836_00000407_1.dimuon.dst',
], clear=True)

