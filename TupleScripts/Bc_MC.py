


from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolTISTOS
from Configurables import DaVinci
from GaudiConf import IOHelper

triggerlines = [
        "L0SingleMuon",
        "L0MuonDecision",
        "L0DiMuonDecision",
        "L0HadronDecision",
        "L0ElectronDecision",
        "L0PhotonDecision",
        #Hlt1 MVA lines
        'Hlt1TrackMVADecision', 
        'Hlt1TwoTrackMVADecision', 
        'Hlt1TrackMVALooseDecision', 
        'Hlt1TwoTrackMVALooseDecision', 
        #Hlt1 track lines    
        'Hlt1TrackAllL0Decision', 
        'Hlt1TrackMuonDecision', 
        'Hlt1TrackMuonNoSPDDecision', 
        #Hlt1 muon lines
        'Hlt1DiMuonHighMassDecision', 
        'Hlt1DiMuonLowMassDecision', 
        'Hlt1SingleMuonNoIPDecision', 
        'Hlt1SingleMuonHighPTDecision', 
        'Hlt1MultiMuonNoL0Decision', 
        'Hlt1MultiMuonNoIPDecision', 
        'Hlt1DiMuonNoL0Decision', 
        'Hlt1DiMuonNoIPDecision', 
        #Hlt1 other lines
        'Hlt1L0AnyDecision', 
        'Hlt1L0AnyNoSPDDecision', 
        'Hlt1TrackMuonMVADecision',
        #Hlt2 muon lines
        'Hlt2SingleMuonDecision',
        'Hlt2SingleMuonHighPTDecision',
        'Hlt2SingleMuonLowPTDecision',
        'Hlt2SingleMuonRareDecision',
        'Hlt2SingleMuonVHighPTDecision',
        #Hlt2 dimuon lines
        'Hlt2DiMuonDecision',
        'Hlt2DiMuonJPsiDecision',
        'Hlt2DiMuonDetachedDecision',
        'Hlt2DiMuonDetachedHeavyDecision',
        'Hlt2DiMuonDetachedJPsiDecision',
        'Hlt2DiMuonDetachedPsi2SDecision'
        'Hlt2DiMuonSoftDecision', #?
        #Hlt2 Too
        'Hlt2TopoMu2BodyBBDT',
        'Hlt2MuTopo3BodyBBDT',
        'Hlt2Topo2BodyDecision', 
        'Hlt2Topo3BodyDecision',
        'Hlt2Topo4BodyDecision',
        'Hlt2TopoMu2BodyDecision',
        'Hlt2TopoMu3BodyDecision',
        'Hlt2TopoMu4BodyDecision',
        'Hlt2TopoMuMu2BodyDecision',
        'Hlt2TopoMuMu3BodyDecision',
        'Hlt2TopoMuMu4BodyDecision'
]

# Stream and stripping line we want to use
stream = 'AllStreams'
line = 'Bc2JpsiHBDTLine'


# Create an ntuple to capture D*+ decays from the StrippingLine line
# dtt = DecayTreeTuple('Bc2JpsiHDetachedLine')
dtt = DecayTreeTuple('Bc2JpsiHBDTLine')

dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)]

# Note that we mark all particles, otherwise the branches won't work
# dtt.Decay =   '[ B_c- -> ^J/psi(1S) ^pi- ]CC'
dtt.Decay =   '[B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+]CC'


dtt.ToolList =  [    "TupleToolGeometry",
                                "TupleToolL0Data",
                                "TupleToolKinematic",
                                "TupleToolPrimaries",
                                "TupleToolEventInfo",
                                "TupleToolTrackInfo",
                                "TupleToolRecoStats",
                                "TupleToolAngles",
                                "TupleToolDira",
                                "TupleToolPid",
                                "TupleToolPropertime",
                                "TupleToolANNPID",
                            ]

tistos = dtt.addTupleTool(TupleToolTISTOS)
tistos.TriggerList = triggerlines


Decay_LoKi = dtt.addTupleTool('LoKi::Hybrid::TupleTool/Decay_LoKi')
Decay_LoKi.Variables = { 
        "RAPIDITY"                 : "Y",
        "ETA"                      : "ETA",
        "PHI"                      : "PHI", 
        "LOKI_DTF_CTAU"            : "DTF_CTAU( 0, True )",
        "LOKI_DTF_CTAUS"           : "DTF_CTAUSIGNIFICANCE( 0, True )",
        "LOKI_DTF_CHI2NDOF"        : "DTF_CHI2NDOF( True )",
        "LOKI_DTF_CTAUERR"         : "DTF_CTAUERR( 0, True )",
        "LOKI_DTF_MASS"            : "DTF_FUN ( M , True )" ,
        "LOKI_DTF_VCHI2NDOF"       : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )" ,
        "LOKI_DTF_CHI2_JpsiConstr" : "DTF_CHI2(  True , 'J/psi(1S)' )",
        "LOKI_MASS_JpsiConstr"     : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
        }

mctruth = dtt.addTupleTool('TupleToolMCTruth/mctruth')
mctruth.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]


# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1


# comment out the following lines for grid submission as will be read out by the job description file (ganga_Run1Data.py)

DaVinci().DataType = '2012'
DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-2'

# Use the local input data
IOHelper().inputFiles([
     './00154431_00000017_5.AllStreams.dst',
     './00154431_00000032_5.AllStreams.dst',
], clear=True)

# from Gaudi.Configuration import FileCatalog
# FileCatalog().Catalogs = [ "xmlcatalog_file:/path/to/myCatalog.xml" ]

