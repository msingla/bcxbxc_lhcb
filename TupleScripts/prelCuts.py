import ROOT 
import cuts


## Path of the tuples
inPath = "../root_files/Bc/" 
outDir = '../PreSelection_rootFile/'

#input file name
file = "DVntuple_Bc_Data__2012_MagMD.root"

## Preliminary cuts defined in file cuts.py
cutsToApply = cuts.prelCuts

#decay tuple under study
decay = 'Bc2JpsiHDetachedLine/DecayTree'

## Get the tuples with the prelimenary cuts
inFile = ROOT.TFile.Open(inPath+file)
inTree = inFile.Get(decay) 

print ('outDir:',outDir)
print ('Cuts:',cutsToApply)
print ('')
print('opening file:', inFile)
print('opening tree:', inTree)


## Create output file after prel. cuts
# gROOT.cd()

outFile = ROOT.TFile(outDir+'DVntuple_Bc_Data__2012_MagMD'+'_prelcut.root','RECREATE')
outTree = inTree.CopyTree(cutsToApply)
outTree.Write('DecayTree')
outFile.Close()

