from Configurables import DaVinci
from GaudiConf import IOHelper
from Gaudi.Configuration import *
from Configurables import DaVinci, CondDB


# Load algorithms
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import LoKi__HDRFilter
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
from StandardParticles import StdAllLooseProtons as Protons

from StandardParticles import StdLooseD02KK as D02KK
from StandardParticles import StdLooseD02KPi as D02KPi
from StandardParticles import StdLooseD02PiPi as D02PiPi

from StandardParticles import StdLooseDplus2KKK as Dplus2KKK
from StandardParticles import StdLooseDplus2KKPi as Dplus2KKPi
from StandardParticles import StdLooseDplus2KPiPi as Dplus2KPiPi
from StandardParticles import StdLooseDplus2PiPiPi as Dplus2PiPiPi

from StandardParticles import StdLooseLambdac2PKPi as Lambdac2PKPi

from StandardParticles import StdLooseDsplus2KKPi as Dsplus2KKPi

# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection, StrippingSelection
from PhysConf.Selections import SelectionSequence, AutomaticData, TupleSelection, Selection




############ Stream and stripping line to get B_c ###############
stream = 'Dimuon'
line = 'Bc2JpsiHBDTLine'

# this will filter out the B_c data and make the algorithm faster (LHCb starter kit)
# strip_sel = StrippingSelection("Strip_sel",
#                                "HLT_PASS('StrippingBc2JpsiHBDTLineDecision')")


strip_sel = LoKi__HDRFilter ( 'stripFilter',
                        Code = "HLT_PASS('StrippingBc2JpsiHBDTLineDecision')",
                        Location= "/Event/Strip/Phys/DecReports")

# telling the algorithm to get B_c data from the below location
Bc_data = AutomaticData('/Event/{0}/Phys/{1}/Particles'.format(stream,line))



#Combine B_c and Xc (D0, Dp, Ds, Lc) into Bstar, where Bstar is our pseudo partcile


# -----------------------
# Bc+D0
# -----------------------

### Bc2D02KPi

BcD02KPi_sel = CombineSelection(
    'BcD02KPi_sel',
    [Bc_data, D02KPi],
    DecayDescriptor= '[B*0 -> B_c+ D~0]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcD02KPi_seq = SelectionSequence('BcD02KPi_seq', TopSelection=BcD02KPi_sel)


### BcD02PiPi

BcD02PiPi_sel = CombineSelection(
    'BcD02PiPi_sel',
    [Bc_data, D02PiPi],
    DecayDescriptor= '[B*0 -> B_c+ D~0]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcD02PiPi_seq = SelectionSequence('BcD02PiPi_seq', TopSelection=BcD02PiPi_sel)


### BcD02KK

BcD02KK_sel = CombineSelection(
    'BcD02KK_sel',
    [Bc_data, D02KK],
    DecayDescriptor= '[B*0 -> B_c+ D~0]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcD02KK_seq = SelectionSequence('BcD02KK_seq', TopSelection=BcD02KK_sel)

# -----------------------
# Bc+Dp
# -----------------------

BcDp2KPiPi_sel = CombineSelection(
    'BcDp2KPiPi_sel',
    [Bc_data, Dplus2KPiPi],
    DecayDescriptor= '[B*~0 -> B_c- D+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcDp2KPiPi_seq = SelectionSequence('BcDp2KPiPi_seq', TopSelection=BcDp2KPiPi_sel)

### BcDp2KKK
BcDp2KKK_sel = CombineSelection(
    'BcDp2KKK_sel',
    [Bc_data, Dplus2KKK],
    DecayDescriptor= '[B*~0 -> B_c- D+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcDp2KKK_seq = SelectionSequence('BcDp2KKK_seq', TopSelection=BcDp2KKK_sel)

### BcDp2KKPi

BcDp2KKPi_sel = CombineSelection(
    'BcDp2KKPi_sel',
    [Bc_data, Dplus2KKPi],
    DecayDescriptor= '[B*~0 -> B_c- D+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcDp2KKPi_seq = SelectionSequence('BcDp2KKPi_seq', TopSelection=BcDp2KKPi_sel)

### BcDp2PiPiPi

BcDp2PiPiPi_sel = CombineSelection(
    'BcDp2PiPiPi_sel',
    [Bc_data, Dplus2PiPiPi],
    DecayDescriptor= '[B*~0 -> B_c- D+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcDp2PiPiPi_seq = SelectionSequence('BcDp2PiPiPi_seq', TopSelection=BcDp2PiPiPi_sel)


# -----------------------
# Bc+Ds
# -----------------------

BcDs_sel = CombineSelection(
    'BcDs_sel',
    [Bc_data, Dsplus2KKPi],
    DecayDescriptor= '[B*~0 -> B_c- D_s+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcDs_seq = SelectionSequence('BcDs_seq', TopSelection=BcDs_sel)


# -----------------------
# Bc+Lc
# -----------------------

BcLc_sel = CombineSelection(
    'BcLc_sel',
    [Bc_data, Lambdac2PKPi],
    DecayDescriptor= '[B*0 -> B_c- Lambda_c+]cc',
    DaughtersCuts={},
    CombinationCut="AALL",
    MotherCut="ALL"
)
BcLc_seq = SelectionSequence('BcLc_seq', TopSelection=BcLc_sel)


# -------------------------------------
# Define function to create an ntuple
# ------------------------------------

def create_tuples(name,input, decay):
    dtt = DecayTreeTuple(name)
    Decay_LoKi = dtt.addTupleTool('LoKi::Hybrid::TupleTool/Decay_LoKi')
    Decay_LoKi.Variables = { 
                "ETA"                      : "ETA",
                "PHI"                      : "PHI",  
                }
    dtt.Inputs = input
    dtt.Decay  = decay
    return dtt


# ------------------------------------------
# Create ntuples for various decay channels
# ------------------------------------------

BcD02KPi_decay = '[ B*0 -> ^(B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+) ^(D~0 -> ^K+ ^pi-) ]CC'
BcD02KPi_dtt   = create_tuples('TupleBstarToBcD02KPi', BcD02KPi_seq.outputLocations(), BcD02KPi_decay )

BcD02PiPi_decay = '[ B*0 -> ^(B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+) ^(D~0 -> ^pi+ ^pi-) ]CC'
BcD02PiPi_dtt   = create_tuples('TupleBstarToBcD02PiPi', BcD02PiPi_seq.outputLocations(), BcD02PiPi_decay )

BcD02KK_decay = '[ B*0 -> ^(B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+) ^(D~0 -> ^K+ ^K-) ]CC'
BcD02KK_dtt   = create_tuples('TupleBstarToBcD02KK', BcD02KK_seq.outputLocations(), BcD02KK_decay )

BcDp2KPiPi_decay = '[ B*~0 -> ^(B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi-) ^(D+ -> ^K- ^pi+ ^pi+) ]CC'
BcDp2KPiPi_dtt    = create_tuples('TupleBstarToBcDp2KPiPi', BcDp2KPiPi_seq.outputLocations(), BcDp2KPiPi_decay )

BcDp2KKK_decay = '[ B*~0 -> ^(B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi-) ^(D+ -> ^K- ^K+ ^K+) ]CC'
BcDp2KKK_dtt   = create_tuples('TupleBstarToBcDp2KKK', BcDp2KKK_seq.outputLocations(), BcDp2KKK_decay )

BcDp2KKPi_decay = '[ B*~0 -> ^(B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi-) ^(D+ -> ^K- ^K+ ^pi+) ]CC'
BcDp2KKPi_dtt   = create_tuples('TupleBstarToBcDp2KKPi', BcDp2KKPi_seq.outputLocations(), BcDp2KKPi_decay )

BcDp2PiPiPi_decay = '[ B*~0 -> ^(B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi-) ^(D+ -> ^pi- ^pi+ ^pi+) ]CC'
BcDp2PiPiPi_dtt   = create_tuples('TupleBstarToBcDp2PiPiPi', BcDp2PiPiPi_seq.outputLocations(), BcDp2PiPiPi_decay )

BcDs_decay = '[ B*0 -> ^(B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+) ^(D_s- -> ^K+ ^K- ^pi-) ]CC'
BcDs_dtt   = create_tuples('TupleBstarToBcDs', BcDs_seq.outputLocations(), BcDs_decay )

BcLc_decay = '[ B*0 -> ^(B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+) ^(Lambda_c+ -> ^K- ^p+ ^pi+) ]CC'
BcLc_dtt   = create_tuples('TupleBstarToBcLc', BcLc_seq.outputLocations(), BcLc_decay )


seq = [BcD02KK_seq.sequence(),
      BcD02KPi_seq.sequence(),
      BcD02PiPi_seq.sequence(),
      BcDp2KKK_seq.sequence(),
      BcDp2KKPi_seq.sequence(),
      BcDp2KPiPi_seq.sequence(),
      BcDp2PiPiPi_seq.sequence(),
      BcDs_seq.sequence(),
      BcLc_seq.sequence(),
      ]

dtt = [BcD02KK_dtt,
      BcD02KPi_dtt,
      BcD02PiPi_dtt,
      BcDp2KKK_dtt,
      BcDp2KKPi_dtt,
      BcDp2KPiPi_dtt,
      BcDp2PiPiPi_dtt,
      BcDs_dtt,
      BcLc_dtt,
      ]



DaVinci().UserAlgorithms += seq
DaVinci().UserAlgorithms += dtt

# DaVinci configuration
DaVinci().EventPreFilters = [strip_sel]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation       # Only ask for luminosity information when not using simulated data
DaVinci().EvtMax = -1

# comment out the following lines for grid submission as will be read out by the job description file (ganga_Run1Data.py)
DaVinci().DataType = '2012'
DaVinci().CondDBtag = 'cond-20141107'
DaVinci().DDDBtag = 'dddb-20130929-1'

#Use the local input data
IOHelper().inputFiles([
     './00041836_00000171_1.dimuon.dst',
     './00041836_00000407_1.dimuon.dst'
     
], clear=True)
