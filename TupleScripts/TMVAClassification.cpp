#include <cstdlib>
#include <iostream>
#include <map>
#include <math.h>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMVA/DataLoader.h"


#include "TMVAGui.C"

#if not defined(__CINT__) || defined(__MAKECINT__)
// needs to be included when makecint runs (ACLIC)
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#endif

void TMVAClassification( TString myMethodList = "BDTG", TString trainOn = "MC" )
{

   TChain* background = new TChain("DecayTree");
   background->Add("../root_files/PreSelection/DVntuple_Bc_Data_2012.root");
   // background->Add("../root_files/PreSelection/DVntuple_Bc_Data__2012_MagMD_prelcut.root");
  
   TChain* signal = new TChain("DecayTree");
   if(trainOn == "MC")signal->Add("../root_files/PreSelection/MC_Bc2JpsiPi_2012_prelcut.root");
   // else signal->Add("../root_files/PreSelection/ms.root");

   //---------------------------------------------------------------
   // This loads the library
   TMVA::Tools::Instance();

   // Default MVA methods to be trained + tested
   std::map<std::string,int> Use;

   // --- Boosted Decision Trees
   Use["BDT"]             = 1; // uses Adaptive Boost
   Use["BDTG"]            = 1; // uses Gradient Boost
   Use["BDTB"]            = 0; // uses Bagging
   Use["BDTD"]            = 0; // decorrelation + Adaptive Boost
   Use["BDTF"]            = 0; // allow usage of fisher discriminant for node splitting 
   // ---------------------------------------------------------------

   std::cout << std::endl;
   std::cout << "==> Start TMVAClassification" << std::endl;

   // Select methods (don't look at this code - not of interest)
   if (myMethodList != "") {
      for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) it->second = 0;

      std::vector<TString> mlist = TMVA::gTools().SplitString( myMethodList, ',' );
      for (UInt_t i=0; i<mlist.size(); i++) {
         std::string regMethod(mlist[i]);

         if (Use.find(regMethod) == Use.end()) {
            std::cout << "Method \"" << regMethod << "\" not known in TMVA under this name. Choose among the following:" << std::endl;
            for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) std::cout << it->first << " ";
            std::cout << std::endl;
            return;
         }
         Use[regMethod] = 1;
      }
   }

   // --------------------------------------------------------------------------------------------------

   // --- Here the preparation phase begins

   // Create a ROOT output file where TMVA will store ntuples, histograms, etc.

   //TString outDir = "plots";
   TString outDir = "../root_files/TMVA/";
 
   TString outfileName = "TMVA_Bc2JpsiPi_" +  myMethodList + "_" + trainOn + "_" + ".root";
   TFile* outputFile = TFile::Open( outfileName, "RECREATE" );



   // Create the factory object. 
   // TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification_" + trainOn + "_" , outputFile,
   //                                             "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" );

   TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification_" + trainOn + "_" , outputFile,
                                               "!V:!Silent:Color:DrawProgressBar:Transformations=I:AnalysisType=Classification" );

   TMVA::DataLoader *dataloader=new TMVA::DataLoader("dataset");

   signal->SetBranchStatus("*",0);  // disable all branches
   signal->SetBranchStatus("*IPCHI2_OWNPV*",1);
   signal->SetBranchStatus("*PT*",1);
   signal->SetBranchStatus("B_cplus_PT",0);
   signal->SetBranchStatus("B_cplus_LOKI_DTF_CTAU",1);
   signal->SetBranchStatus("B_cplus_LOKI_DTF_VCHI2NDOF",1);
   signal->SetBranchStatus("B_cplus_LOKI_DTF_MASS",1);

   signal->SetBranchStatus( "nVeloTracks",1 );
   signal->SetBranchStatus( "nTracks" ,1);
   signal->SetBranchStatus( "nPVs" ,1);
   signal->SetBranchStatus( "B_cplus_M",1 );


   background->SetBranchStatus("*",0);  // disable all branches
   background->SetBranchStatus("*IPCHI2_OWNPV*",1);
   background->SetBranchStatus("*PT*",1);
   background->SetBranchStatus("B_cplus_PT",0);
   background->SetBranchStatus("B_cplus_LOKI_DTF_CTAU",1);
   background->SetBranchStatus("B_cplus_LOKI_DTF_VCHI2NDOF",1);
   background->SetBranchStatus("B_cplus_LOKI_DTF_MASS",1);
   background->SetBranchStatus( "nVeloTracks",1 );
   background->SetBranchStatus( "nTracks",1 );
   background->SetBranchStatus( "nPVs",1 );
   background->SetBranchStatus( "B_cplus_M",1 );

   // Define the input variables that shall be used for the MVA training

   dataloader->AddVariable( "B_cplus_IPCHI2_OWNPV := sqrt(B_cplus_IPCHI2_OWNPV)","B_cplus_IPCHI2_OWNPV", "", 'F' );
   dataloader->AddVariable( "muplus_IPCHI2_OWNPV := sqrt(muplus_IPCHI2_OWNPV)","muplus_IPCHI2_OWNPV", "", 'F' );
   dataloader->AddVariable( "muminus_IPCHI2_OWNPV:=sqrt(muminus_IPCHI2_OWNPV)","muminus_IPCHI2_OWNPV", "", 'F' );
   dataloader->AddVariable( "J_psi_1S_IPCHI2_OWNPV:=sqrt(J_psi_1S_IPCHI2_OWNPV)","J_psi_1S_IPCHI2_OWNPV", "", 'F' );
   dataloader->AddVariable( "piplus_IPCHI2_OWNPV:=sqrt(piplus_IPCHI2_OWNPV)","piplus_IPCHI2_OWNPV", "", 'F' );

   dataloader->AddVariable( "muplus_PT","muplus_PT","", 'F' );
   dataloader->AddVariable( "muminus_PT","muminus_PT","", 'F' );
   dataloader->AddVariable( "J_psi_1S_PT","J_psi_1S_PT","", 'F' );
   dataloader->AddVariable( "piplus_PT","piplus_PT","", 'F' );

   dataloader->AddVariable( "B_cplus_LOKI_DTF_CTAU", "B_cplus_LOKI_DTF_CTAU", "", 'F' );
   dataloader->AddVariable( "B_cplus_LOKI_DTF_VCHI2NDOF", "B_cplus_LOKI_DTF_VCHI2NDOF", "", 'F' );


   // You can add so-called "Spectator variables", which are not used in the MVA training, 
   dataloader->AddSpectator( "nVeloTracks" );
   dataloader->AddSpectator( "nTracks" );
   dataloader->AddSpectator( "nPVs" );
   dataloader->AddSpectator( "B_cplus_M" );

     
   // global event weights per tree (see below for setting event-wise weights)
   Double_t signalWeight     = 1.0;
   Double_t backgroundWeight = 1.0;

   // Apply additional cuts on the signal and background samples (can be different)
   TCut mycutSig; 
   if(trainOn == "MC") mycutSig = "B_cplus_LOKI_DTF_MASS > 6000  && B_cplus_LOKI_DTF_MASS < 6350 ";

   TCut mycutBkg = "B_cplus_LOKI_DTF_MASS > 6350";
   
 
   dataloader->AddSignalTree    ( signal,     signalWeight     );
   dataloader->AddBackgroundTree( background, backgroundWeight );
   dataloader->PrepareTrainingAndTestTree( mycutSig, mycutBkg,
                                        "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" );
  //If nTrain Signal=0 and nTest Signal=0, the signal sample is split in half for training and testing

   // dataloader->SetSignalWeightExpression("weight");

   // ---- Book MVA methods

   // Boosted Decision Trees
   if (Use["BDTG"]) // Gradient Boost
      factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDTG",
                           "!H:!V:NTrees=500:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=40:MaxDepth=3:NegWeightTreatment=Pray" );

   if (Use["BDT"])  // Adaptive Boost
      factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT",
                           "!H:!V:NTrees=500:MinNodeSize=3.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=80" );

   if (Use["BDTB"]) // Bagging
      factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTB",
                           "!H:!V:NTrees=400:BoostType=Bagging:SeparationType=GiniIndex:nCuts=20" );

   if (Use["BDTD"]) // Decorrelation + Adaptive Boost
      factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTD",
                           "!H:!V:NTrees=400:MinNodeSize=5%:MaxDepth=3:BoostType=AdaBoost:SeparationType=GiniIndex:nCuts=20:VarTransform=Decorrelate" );

   if (Use["BDTF"])  // Allow Using Fisher discriminant in node splitting for (strong) linearly correlated variables
      factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTMitFisher",
                           "!H:!V:NTrees=50:MinNodeSize=2.5%:UseFisherCuts:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20" );

   // Train MVAs using the set of training events
   factory->TrainAllMethods();

   // ---- Evaluate all MVAs using the set of test events
   factory->TestAllMethods();

   // ----- Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();

   // --------------------------------------------------------------

   // Save the output
   outputFile->Close();

   std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
   std::cout << "==> TMVAClassification is done!" << std::endl;

   delete factory;
   delete dataloader;

   // Launch the GUI for the root macros
   if (!gROOT->IsBatch()) TMVA::TMVAGui( outfileName );
 

}



