using namespace RooFit;
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooClassFactory.h"
#include "TROOT.h"
#include "RooCBShape.h"
#include "RooStats/SPlot.h"

#include "stdio.h"
#include "time.h"

using namespace RooFit;
using namespace RooStats;

void MassFitBc(){
   
#ifdef __CINT__
    gROOT->ProcessLine(".x lhcbStyle.C") ;
#endif
    double B_cplus_LOKI_DTF_MASS ;  // Bcplus_MASS for ms file

    // int n=0;

    TFile* file = TFile::Open("../PreSelection_rootFile/DVntuple_Bc_Data_2012.root");
    TTree* BcTree = (TTree*) file->Get("DecayTree");
    // int ent = BcTree->GetEntries();
    // cout << "no. of entries: " << ent <<endl;

    TChain* fchain = new TChain("DecayTree");
    fchain->Add( file->GetName() ) ;


    int nentries = fchain->GetEntries();

    //=========================== Combinatorial shape ====================
    RooRealVar *m 			             = new RooRealVar("B_cplus_LOKI_DTF_MASS","M(J/#psi#pi^{+})",6150,6550,"MeV/c^{2}");
    RooRealVar *BkgMSlope1               = new RooRealVar("c_{exp}1","Slope1 of mass bkg", -0.0005, -1.0, 0.5);
    RooAbsPdf  *BkgMPdf                  = new RooExponential("BkgMPdf", "BkgMPdf", *m, *BkgMSlope1);


  //====================================================================
  // Mass PDF
  //====================================================================
    RooRealVar aL0("aL0", "aL0",  2.255,  -10, 10);
    RooRealVar aL1("aL1", "aL1",  0.02851,  -10, 10);
    RooRealVar aL2("aL2", "aL2", 0.0007267, -10, 10);
    aL0.setConstant(true);
    aL1.setConstant(true);
    aL2.setConstant(true);

    RooRealVar aR0("aR0", "aR0",  1.384,    -10, 10);
    RooRealVar aR1("aR1", "aR1", 0.158, -10, 10);
    RooRealVar aR2("aR2", "aR2", -0.00001454, -10, 10);
    aR0.setConstant(true);
    aR1.setConstant(true);
    aR2.setConstant(true);
  
  
    RooRealVar *SigM 			        = new RooRealVar("m_{B_{c}^{+}}","M(J/#psi#pi{+})",6266.53,6150,6550,"MeV/#it{c}^{2}");
    RooRealVar *sigma_CB   		        = new RooRealVar("#sigma_{m}","signal mass resolution",16.4,0.1,20.,"MeV/#it{c}^{2}");//used to be 13.4 //comes from dector resolution MS
    RooUnblindUniform *SigM_Blind       = new RooUnblindUniform("SigM_Blind","Blinded mass", "BcValais", 10., *SigM ); // mean
    // RooRealVar *SigM_Blind              = new RooRealVar("SigM_Blind","Blinded mass", 6250,6150,6550 ); // mean
    RooRealVar *nL         		        = new RooRealVar("n_{L}","nL",1.0);
    RooFormulaVar *alphaL     	        = new RooFormulaVar("alpha_{L}","alphaL","@0+@1*@3-@2*@3*@3",RooArgList(aL0, aL1, aL2, *sigma_CB));    
    RooRealVar *nR         		        = new RooRealVar("n_{R}","nR",6.0);
    RooFormulaVar *alphaR     	        = new RooFormulaVar("alpha_{R}","alphaR","@0+@1*@3-@2*@3*@3",RooArgList(aR0, aR1, aR2, *sigma_CB));
    RooAbsPdf  *CB         		    = new RooCBShape("CBL","CBL",*m,*SigM_Blind,*sigma_CB,*alphaL,*nL);
    // RooAbsPdf  *CBR         		    = new RooCBShape("CBR","CBR",*m,*SigM_Blind,*sigma_CB,*alphaR,*nR); 
    // RooRealVar *frac                    = new RooRealVar("frac", "", 0.5);
    // RooAbsPdf  *CB         		        = new RooAddPdf("CB","CB",RooArgList(*CBL, *CBR), RooArgList(*frac)) ; 
    // RooAbsPdf  *CB         		    = new MyCB("CB","CB",*m,*SigM_Blind,*sigma_CB,*alphaL,*nL,*alphaR,*nR);
    RooRealVar *nSig                    = new RooRealVar("N_{Sig}","number of sig events", 50000, 0, 1.0e+8 );
    RooRealVar *nBkg                    = new RooRealVar("N_{Bkg}","number of bkg events", 100000, 0, 1.0e+8 );    
    RooAbsPdf  *MassPdf                 = new RooAddPdf("MassPdf","mass pdf",RooArgList(*CB,*BkgMPdf),RooArgList(*nSig, *nBkg));

    //====================fit binned data========================
    TH1D *hist = new TH1D("hist", "hist", 40,6150,6550);
    BcTree->Project("hist", "B_cplus_LOKI_DTF_MASS");
    RooDataHist *binData = new RooDataHist("binData", "", *m, hist);
    binData->Print();

    //=========================================
    RooFitResult* fitres = MassPdf->fitTo(*binData,Extended(true), Minos(true), Save());        //@@@
    fitres->Print("V");

    RooDataSet *Data = new RooDataSet("Data", "", *m, Import(*BcTree));  
    // Data->Print();

    //=========================================
    TCanvas* MyCan = new TCanvas("MyCan","",600,600);
	RooPlot *mframe = m->frame(Bins(80));

	binData->plotOn(mframe,Name("TheData"));
	MassPdf->plotOn(mframe,Name("TheSig"),Components(*CB), LineColor(kRed-4), LineStyle(3));
    MassPdf->plotOn(mframe,Name("TheBkg"),Components(*BkgMPdf), LineColor(kGreen), LineStyle(kDashed));
	MassPdf->plotOn(mframe,Name("TheMode"));
         
    RooArgSet* showParams = new RooArgSet( *nSig, *nBkg, *sigma_CB, *SigM  );
	MassPdf->paramOn(mframe, Layout(0.55,0.92,0.92), Parameters(*showParams), ShowConstants(true) );

    //=========add pull=====================
    RooHist *hpull = mframe->pullHist();  // Construct a histogram with the pulls of the data w.r.t the curve
    RooPlot *pullframe = m->frame(Title("Pull Distribution")); // creating a new frame to draw pull distribution
    pullframe->addPlotable(hpull, "P");  // add the pull distribution to the frame
    pullframe->addPlotable(hpull, "L3");
    MyCan->Divide(1,2);
    MyCan->cd(1);
    gPad->SetPad(0.02, 0.35, 0.98, 0.98);
    
    TPaveText *paraBox = dynamic_cast<TPaveText*>(mframe->findObject("MassPdf_paramBox"));
    paraBox->SetBorderSize(0);
  
    TLatex *myTex = new TLatex(0.56, 0.56,
			     "#splitline{LHCb}{#scale[1]{Run1 Data}}");

    myTex->SetNDC();  

    myTex->SetTextSize(0.06);
    myTex->SetTextFont(132);
    myTex->SetTextAlign(11);
    mframe->addObject( myTex  );
  //====================================================================
  // Chi2
  //====================================================================
    Double_t chi2 = mframe->chiSquare("TheMode", "TheData");  //calculate the chi2 of the curve w.r.t. the histogram
  
    char text2[1024];
    sprintf(text2,"#chi^{2}/ndf: %4.2f", chi2 );
    TLatex *L2 = new TLatex(0.16,0.73,text2);
    L2->SetTextSize(0.04);
    L2->SetNDC();
    mframe->addObject( L2 );  

    myTex->SetTextSize(0.05);
    mframe->addObject( myTex );

    mframe->Draw();
    //========draw pull=================
    MyCan->cd(2);
    gPad->SetPad(0.02, 0.01, 0.98, 0.35);
   // gPad->SetTicks(0,1);
    hpull->SetFillColor(0);
    hpull->SetLineWidth(0);
    hpull->SetMarkerSize(0.5);
    hpull->SetMarkerStyle(20);
    hpull->SetFillStyle(0);
    pullframe->GetYaxis()->SetTitle("Pulls");
    pullframe->GetYaxis()->SetTitleSize(0.10);
    pullframe->GetYaxis()->SetTitleOffset(0.5);
    pullframe->GetYaxis()->CenterTitle();
    pullframe->GetYaxis()->SetLabelSize(0.08);
    pullframe->GetXaxis()->SetLabelSize(0.08);
    pullframe->Draw();

    TString fileName = "Bc2JpsiPi_exp_6150_bdt0.82_addpull";
    // TString epsName = fileName + ".eps";
    TString pngName = fileName + ".png";
    TString pdfName = fileName + ".pdf";
 
    // MyCan->Print( epsName );
    MyCan->Print( pngName );
    MyCan->Print( pdfName );

}
