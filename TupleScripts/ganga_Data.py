# Inspired from B2Kstarmumu Analysis Script

name = "Bc_Data_"


bkkpath = {
        '2011' : {
                'MD' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST',
                'MU' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST'
                },
                
        '2012' : { 
                'MD' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST',
                'MU' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/DIMUON.DST',
                },

        '2015' : {   'MD' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST',
                 'MU' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST',
                },

        '2016' : {   'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST',
                 'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST',
                }, 

        '2017' : {   
                'MD' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST',  
                 'MU' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST',
                }, 
        
        '2018' : { 'MD' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST',
                 'MU' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST'}
}


myBackend = Dirac()
# myBackend = Local()

### Splitter
mySplitter = SplitByFiles()
mySplitter.filesPerJob = 20
mySplitter.maxFiles = -1

### Testing
# mySplitter.filesPerJob = 1
# mySplitter.maxFiles = 5

for year, pols in bkkpath.items():
    for pol in pols:
                print("Submiting year: %s - polarity: %s" % (year, pol))

                myJobName = name
                myOutputdata =[DiracFile('*.root')]
                myJobComment = year+"_"+pol

                path = pols[pol]
                bkkquery = BKQuery(path,dqflag=['OK'])
                data = bkkquery.getDataset()
                if not data:
                        print("No data for ",data)
                        break
                        

                myApplication = GaudiExec()
                myApplication.directory = '/afs/cern.ch/work/m/msingla/Bc_reconstruction/DaVinciDev_v46r2' #Use a recent version of DaVinci as we are just reading a DST
                myApplication.platform = 'x86_64_v2-centos7-gcc11-opt'
                myApplication.options = ["JPsiPi2Bc_Run1Data.py"] # This is the name of your options file #Bc_Run1Data.py is to filter Bc from Run1 Data
                # myApplication.options = ["BcXc.py"]  
                # data = data[0:10] # Just for testing, comment out to send run over all the data
                        
                j = Job (
                        name         = myJobComment,
                        comment      = myJobName,
                        application  = myApplication,
                        splitter     = mySplitter,
                        # postprocessors       = myMerger,  # I tend to merge things myself, but you can use ganga Mergers to combine the output of ganga subjobs
                        # inputfiles = _inputsandbox,
                        outputfiles   = myOutputdata,
                        backend      = myBackend,
                        inputdata    = data,
                        do_auto_resubmit = True,
                        )

                # j.application.extraOpts = 'from Configurables import DaVinci\nDaVinci().DataType="%s"'%(year)
                j.application.extraOpts = ( "from Configurables import DaVinci, CondDB \n" 
                                            "DaVinci().DataType  = '{year}' \n"
                                            "CondDB(LatestGlobalTagByDataType='{year}') \n"
                                            "DaVinci().TupleFile = 'DVntuple_{name}_{year}_Mag{pol}.root' \n"
                                          ).format(year = year,
                                                   pol = pol,
                                                   name = name)


                #j.application = myApplication
                
                print("... ",pol," out...")
                #to submit the job
                queues.add(j.submit)

print("... Done!")



